# R script to locate English-speaking therapists in Berlin on a Google Maps
### 2021-01-15 by Ed Parsadanyan

Description: R script to load English-speaking therapists and place them on a Google map

SCV-files were parsed using https://stats-consult.shinyapps.io/shinyparser_poc/

Final map can be found here: https://www.google.com/maps/d/viewer?mid=1B28MXzs6ztZP91lKekZmMO3cwqFbGa_v

Please note that the final map needs some manual cleaning due to issues in address line
